# terrapingit

![](https://img.shields.io/badge/written%20in-C%2B%2B%20%28Qt%2C%20libgit2%29-blue)

A graphical git client.

The application is designed to mimic TortoiseHg.

The 0.0.20171023 release is pre-alpha - it can view history, and make commits, but is not really useful.

## License

GPLv2
- This project includes GPLv2-licensed images from the TortoiseHg project
- The application icon is based on [work by Matteo Della Chiesa](https://thenounproject.com/term/turtle/159969/) (CC-BY 3.0 US)

## Changelog

2017-10-23 0.0.20171023
- Initial development
- [⬇️ terrapingit-0.0.20171023-src.zip](dist-archive/terrapingit-0.0.20171023-src.zip) *(38.24 KiB)*

